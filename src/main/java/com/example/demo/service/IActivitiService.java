package com.example.demo.service;

import com.example.demo.entity.YwProcessInfoPO;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

/**
 * @author ZB3436.xiongshibao
 */
public interface IActivitiService {

    /**
     * 流程部署
     * @param modelId 模型ID
     * @return  部署状态值
     * @throws IOException  异常
     */
    String deploymentProcess(String modelId) throws IOException;

    /**
     * 启动流程定义
     * @param processesId 流程ID
     * @param businessKey 关联ID
     * @param variables 流程变量map
     * @return  流程实例
     */
    ProcessInstance startProcessInstance(String processesId , String businessKey , Map<String , Object> variables);

    /**
     * 完成节点任务
     * @param taskId taskId
     * @param variables 流程变量
     */
    void completeTask(String taskId , Map<String, Object> variables);

    /**
     * 删除流程实例 根据流程ID
     * `act_ge_bytearray`
     * `act_re_deployment`
     * `act_re_procdef`
     * @param deploymentId  部署ID
     * @param deleteReason  删除原因
     */
    void deleteTaskProcessInstance(String deploymentId, String deleteReason);

    /**
     *  驳回节点任务
     * @param taskId    任务ID
     * @param deleteReason  驳回原因
     */
    void turndownTaskInstance(String taskId , String deleteReason);

    /**
     * 终止节点任务-从当前节点跳转值尾节点
     * @param taskId    任务ID
     * @param endTaskReason 终止原因
     */
    void endTaskInstance(String taskId,String endTaskReason);


    /**
     * 根据业务id 创建流程实例
     * @param businessKey 业务id
     * @param applyType 申请类型 特殊：合同申请类型（运营类、非运营类）
     */
    void startProcessInstance(String businessKey , String applyType);

    /**
     * 完成节点任务
     *  审核通过/驳回任务
     * @param processInfo 流程信息
     */
    void completeTask(YwProcessInfoPO processInfo);

    /**
     * 终止流程
     * @param processInfoPO 流程信息
     */
    void terminateProcessInstance(YwProcessInfoPO processInfoPO);

    /**
     * 上传文件 部署工作流
     * @param multipartFile 文件
     * @return string
     */
    String updateFile(MultipartFile multipartFile);
}
