package com.example.demo.service.impl;

import com.example.demo.controller.JumpCmd;
import com.example.demo.entity.YwProcessInfoPO;
import com.example.demo.service.IActivitiService;
import com.example.demo.util.ActivitiUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.editor.language.json.converter.BpmnJsonConverter;
import org.activiti.engine.*;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.impl.RepositoryServiceImpl;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.impl.pvm.PvmTransition;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.impl.pvm.process.ProcessDefinitionImpl;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.Model;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.zip.ZipInputStream;


/**
 * @author ZB3436.xiongshibao
 */
@Service
@Slf4j
public class ActivitiServiceImpl implements IActivitiService {

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private FormService formService;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private ProcessEngineConfiguration processEngineConfiguration;

    //移交用房申请
    private static final String DELIVERY_HOUSE_APPLY = "DELIVERY_HOUSE_APPLY";
    //合同签署申请-运营类
    private static final String CONTRACT_SIGN_APPLY_AGENCY = "CONTRACT_SIGN_APPLY_AGENCY";
    //合同签署申请-非运营类
    private static final String CONTRACT_SIGN_APPLY_NORMAL = "CONTRACT_SIGN_APPLY_NORMAL";
    //物业资产申报
    private static final String PROPERTY_APPLY = "PROPERTY_APPLY";


    @Override
    public String deploymentProcess(String modelId) throws IOException {
        //获取模型
        Model modelData = repositoryService.getModel(modelId);
        byte[] bytes = repositoryService.getModelEditorSource(modelData.getId());

        if (bytes == null) {
            return "模型数据为空，请先设计流程并成功保存，再进行发布。";
        }
        JsonNode modelNode = new ObjectMapper().readTree(bytes);

        BpmnModel model = new BpmnJsonConverter().convertToBpmnModel(modelNode);
        if (model.getProcesses().size() == 0) {
            return "数据模型不符要求，请至少设计一条主线流程。";
        }
        byte[] bpmnBytes = new BpmnXMLConverter().convertToXML(model);

        //发布流程
        String processName = modelData.getName() + ".bpmn20.xml";
        Deployment deployment = repositoryService.createDeployment()
                .name(modelData.getName())
                .addString(processName, new String(bpmnBytes, "UTF-8"))
                .deploy();
        modelData.setDeploymentId(deployment.getId());
        repositoryService.saveModel(modelData);

        return "流程发布成功";
    }

    @Override
    public ProcessInstance startProcessInstance(String processesId, String businessKey, Map<String, Object> variables) {
        return runtimeService.startProcessInstanceById(processesId, businessKey, variables);
    }

    @Override
    public void completeTask(String taskId ,Map<String, Object> variables) {
        Task task = this.taskService.createTaskQuery().taskId(taskId).singleResult();
        taskService.complete(task.getId(), variables);
    }

    @Override
    public void deleteTaskProcessInstance(String deploymentId, String deleteReason) {

        repositoryService.deleteDeployment(deploymentId, true);

    }

    @Override
    public void turndownTaskInstance(String taskId , String reason) {

        Task taskTemp = this.taskService.createTaskQuery().taskId(taskId).singleResult();
        //获取历史节点
        List<HistoricTaskInstance> list = this.historyService.createHistoricTaskInstanceQuery()
                .processDefinitionId(taskTemp.getProcessDefinitionId())
                .orderByTaskCreateTime().asc()
                .list();
        //获取流程实例
        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()
                .processInstanceId(taskTemp.getProcessInstanceId())
                .singleResult();
        //获取流程定义 找到上级节点
        ProcessDefinition processDefinition = repositoryService.getProcessDefinition(taskTemp.getProcessDefinitionId());
        Map<String, String> targetAndSourceRef = ActivitiUtils.getTargetAndSourceRef(repositoryService, (ProcessDefinitionEntity) processDefinition);

        String runningActivityId = processInstance.getActivityId();
        String fatherActivityId = targetAndSourceRef.get(runningActivityId);
        //根据上级节点ID 找到历史节点
        Optional<HistoricTaskInstance> optionalHistoricTaskInstance = list.stream().filter(e -> e.getTaskDefinitionKey().equals(fatherActivityId)).findFirst();

        if(optionalHistoricTaskInstance.isPresent()){
            HistoricTaskInstance hisTask = optionalHistoricTaskInstance.get();
            //进而获取流程实例
            ProcessInstance instance = runtimeService.createProcessInstanceQuery().processInstanceId(hisTask.getProcessInstanceId()).singleResult();
            //取得流程定义
            ProcessDefinitionEntity definition = (ProcessDefinitionEntity) repositoryService.getProcessDefinition(hisTask.getProcessDefinitionId());
            //获取历史任务的Activity
            ActivityImpl hisActivity = definition.findActivity(hisTask.getTaskDefinitionKey());
            //实现跳转
            ManagementService managementService = processEngineConfiguration.getManagementService();
            JumpCmd jumpCmd = new JumpCmd(instance.getId(), hisActivity.getId());
            managementService.executeCommand(jumpCmd.setJumpReason(reason));
        }

    }

    @Override
    public void endTaskInstance(String taskId , String endTaskReason) {
        //  当前任务
        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();

        try {
            ActivityImpl end = findActivitiImpl(taskId, "end");

            //进而获取流程实例
            ProcessInstance instance = runtimeService.createProcessInstanceQuery().processInstanceId(task.getProcessInstanceId()).singleResult();

            ManagementService managementService = processEngineConfiguration.getManagementService();
            JumpCmd jumpCmd = new JumpCmd(instance.getId(), end.getId());
            managementService.executeCommand(jumpCmd.setJumpReason(endTaskReason));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void startProcessInstance(String businessKey, String applyType) {

        if (StringUtils.isEmpty(applyType)){
            log.info("业务办理-申请类型为空，业务id："+businessKey);
        }

        String bpmnStr = "";

        switch (applyType){
            case DELIVERY_HOUSE_APPLY:
                //移交用房申请
                bpmnStr = "deliveryHourseApply";
                break;
            case CONTRACT_SIGN_APPLY_AGENCY:
                //合同签署-运营类
                bpmnStr = "contractSignApply2";
                break;
            case CONTRACT_SIGN_APPLY_NORMAL:
                //合同签署-非运营类
                bpmnStr = "contractSignApply1";
                break;
            case PROPERTY_APPLY:
                //物业资产申报
                bpmnStr = "propertyApply";
                break;
            default:
                break;
        }

        List<ProcessDefinition> list = this.repositoryService.createProcessDefinitionQuery()
                .processDefinitionKey(bpmnStr)
                .orderByProcessDefinitionVersion()
                .desc()
                .list();

        if(list.isEmpty()){
            throw new RuntimeException("流程定义为空，请输入正确的流程定义名称或联系管理员");
        }

        ProcessDefinition processDefinition = list.get(0);

        this.runtimeService.startProcessInstanceByKey(processDefinition.getKey() , businessKey);
        log.info("业务ID："+businessKey +"已开启流程，流程名称：" + processDefinition.getName());

    }

    @Override
    public void completeTask(YwProcessInfoPO processInfo) {

        if(null == processInfo || null == processInfo.getBusinessKey()){
            log.info("终止流程失败，业务对象为空");
            return;
        }

        Task task = this.taskService.createTaskQuery()
                .processInstanceBusinessKey(processInfo.getBusinessKey())
                .singleResult();

        Map<String , Object> param = new HashMap<>();
        param.put("result" , processInfo.getResult());

        this.taskService.addComment(task.getId() , task.getProcessInstanceId() , processInfo.getComment());
        this.taskService.complete(task.getId() , param);


    }

    @Override
    public void terminateProcessInstance(YwProcessInfoPO processInfoPO) {

        if(null == processInfoPO || null == processInfoPO.getBusinessKey()){
            log.info("终止流程失败，业务对象为空");
            return;
        }

        Task task = this.taskService.createTaskQuery()
                .processInstanceBusinessKey(processInfoPO.getBusinessKey())
                .singleResult();

        try {
            ActivityImpl end = findActivitiImpl(task.getId(), "end");

            //进而获取流程实例
            ProcessInstance instance = this.runtimeService.createProcessInstanceQuery().processInstanceId(task.getProcessInstanceId()).singleResult();

            this.taskService.addComment(task.getId() , task.getProcessInstanceId() , processInfoPO.getComment());

            ManagementService managementService = processEngineConfiguration.getManagementService();
            JumpCmd jumpCmd = new JumpCmd(instance.getId(), end.getId());
            jumpCmd.setJumpReason("terminated");
            managementService.executeCommand(jumpCmd);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @SneakyThrows
    @SuppressWarnings("")
    @Override
    public String updateFile(MultipartFile multipartFile) {

//        获取流程引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        获取RepositoryService
        RepositoryService repositoryService = processEngine.getRepositoryService();
//        流程部署
//        读取资源包文件，构造成inputStream
//        InputStream inputStream = this.getClass()
//                .getClassLoader()
//                .getResourceAsStream("bpmn/evection.zip");


        InputStream inputStream = multipartFile.getInputStream();

//        用inputStream 构造ZipInputStream
        ZipInputStream zipInputStream = new ZipInputStream(inputStream);
//        使用压缩包的流进行流程的部署
        Deployment deploy = repositoryService.createDeployment()
                .addZipInputStream(zipInputStream)
                .deploy();
        System.out.println("流程部署id="+deploy.getId());
        System.out.println("流程部署的名称="+deploy.getName());



        return deploy.getId() + "==" + deploy.getName();
    }

    /**
     * 根据任务ID和节点ID获取活动节点 <br>
     *
     * @param taskId
     *            任务ID
     * @param activityId
     *            活动节点ID <br>
     *            如果为null或""，则默认查询当前活动节点 <br>
     *            如果为"end"，则查询结束节点 <br>
     *
     * @return 活动节点
     * @throws Exception
     */
    private ActivityImpl findActivitiImpl(String taskId, String activityId)
            throws Exception {
        // 取得流程定义
        ProcessDefinitionEntity processDefinition = findProcessDefinitionEntityByTaskId(taskId);

        // 获取当前活动节点ID
        if (StringUtils.isEmpty(activityId)) {
            activityId = findTaskById(taskId).getTaskDefinitionKey();
        }

        // 根据流程定义，获取该流程实例的结束节点
        if ("END".equals(activityId.toUpperCase())) {
            for (ActivityImpl activityImpl : processDefinition.getActivities()) {
                List<PvmTransition> pvmTransitionList = activityImpl
                        .getOutgoingTransitions();
                if (pvmTransitionList.isEmpty()) {
                    return activityImpl;
                }
            }
        }

        // 根据节点ID，获取对应的活动节点
        ActivityImpl activityImpl = ((ProcessDefinitionImpl) processDefinition)
                .findActivity(activityId);

        return activityImpl;
    }

    /**
     * 根据任务ID获取流程定义
     *
     * @param taskId
     *            任务ID
     * @return 流程定义
     * @throws Exception 实例不存在异常
     */
    private ProcessDefinitionEntity findProcessDefinitionEntityByTaskId(
            String taskId) throws Exception {
        // 取得流程定义
        ProcessDefinitionEntity processDefinition = (ProcessDefinitionEntity) ((RepositoryServiceImpl) repositoryService)
                .getDeployedProcessDefinition(
                        findTaskById(taskId).getProcessDefinitionId());

        if (processDefinition == null) {
            throw new Exception("流程定义未找到!");
        }

        return processDefinition;
    }

    /**
     * 根据任务ID获得任务实例
     *
     * @param taskId
     *            任务ID
     * @return  任务实例
     * @throws Exception    实例不存在异常
     */
    private TaskEntity findTaskById(String taskId) throws Exception {
        TaskEntity task = (TaskEntity) taskService.createTaskQuery().taskId(
                taskId).singleResult();
        if (task == null) {
            throw new Exception("任务实例未找到!");
        }
        return task;
    }

}
