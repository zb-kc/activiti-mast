package com.example.demo.listener;

import org.activiti.engine.HistoryService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.impl.persistence.entity.HistoricDetailVariableInstanceUpdateEntity;
import org.activiti.engine.impl.persistence.entity.VariableInstance;
import org.activiti.engine.runtime.Execution;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 连线监听器
 * @author ZB3436.xiongshibao
 */
public class MyActivitiListener implements ExecutionListener {

    @Override
    public void notify(DelegateExecution execution) throws Exception {

        ProcessEngine defaultProcessEngine = ProcessEngines.getDefaultProcessEngine();
        HistoryService historyService = defaultProcessEngine.getHistoryService();
        RuntimeService runtimeService = defaultProcessEngine.getRuntimeService();

        System.out.println("============executionListener start============");
        String eventName = execution.getEventName();
        String currentActivitiId = execution.getCurrentActivityId();
        System.out.println("事件名称:" + eventName);
        System.out.println("ActivitiId:" + currentActivitiId);
        System.out.println("============executionListener  end============");

        if ("start".equals(eventName)) {
            System.out.println("start====开始=====");
        }else if ("end".equals(eventName)) {
            System.out.println("end====结束=====");
        } else if ("take".equals(eventName)) {
            //会签处理
            System.out.println("take====经过=====");

            String processStatus = "会签同意";

            Map<String , Object> param  = new HashMap<>();

            String processInstanceId = execution.getProcessInstanceId();
            //会签结果判断开始
            //包含网关流程 若三个会签人中有一个驳回，则网关走向驳回

            List<Execution> executionList = runtimeService.createExecutionQuery()
                    .parentId(processInstanceId)
                    .list();

            List<String> historyTaskInstanceList = new ArrayList<>();
            for (Execution executionTemp : executionList) {
                String activityId = executionTemp.getId();
                HistoricTaskInstance historicTaskInstance = historyService.createHistoricTaskInstanceQuery()
                        .executionId(activityId)
                        .singleResult();
                historyTaskInstanceList.add(historicTaskInstance.getId());
            }

            List<String> activityIdList = new ArrayList<>();
            String sql = "select ID_,TASK_ID_ from ACT_HI_ACTINST where TASK_ID_ = 'PLACEHOLDER'";
            for (String historyInstanceIdTemp : historyTaskInstanceList) {

                String placeholder = sql.replace("PLACEHOLDER", historyInstanceIdTemp);
                HistoricActivityInstance historicActivityInstance = historyService.createNativeHistoricActivityInstanceQuery().sql(placeholder).list().get(0);
                activityIdList.add(historicActivityInstance.getId());
            }

            VariableInstance result = execution.getVariableInstance("result");
            String textValue1 = result.getTextValue();
            if(!"同意".equals(textValue1)){
                execution.setVariable("result" , textValue1);
                return;
            }

            for (String activityId : activityIdList) {
                HistoricDetailVariableInstanceUpdateEntity temp = (HistoricDetailVariableInstanceUpdateEntity)historyService.createHistoricDetailQuery()
                        .activityInstanceId(activityId)
                        .singleResult();
                //因为监听器监听的是最后一个连线，所以最后一个userTask获取其填写内容的方式为execution.getVariableInstance("result")。还在缓存中，没有写入数据库
                if(null == temp){
                    continue;
                }

                String name = temp.getName();
                String textValue = temp.getTextValue();
                if ("result".equals(name) && !"同意".equals(textValue)){
                    processStatus = "驳回，材料有问题";
                    break;
                }
            }

            //会签结果判断结束
            execution.setVariable("result" , processStatus);

        }

    }
}
