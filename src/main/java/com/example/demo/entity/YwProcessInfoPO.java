package com.example.demo.entity;

import lombok.Data;

/**
 * 业务流程状态PO类
 * @author ZB3436.xiongshibao
 */
@Data
public class YwProcessInfoPO {

    /**
     * 流程实例id
     */
    private String procInstId;
    /**
     * 活动节点实例id
     */
    private String actInstId;
    /**
     * 活动节点名称
     */
    private String actName;
    /**
     * 活动节点批注信息
     */
    private String text;
    /**
     * 活动节点开始时间
     */
    private String startTimeStr;
    /**
     * 活动节点结束时间
     */
    private String endTimeStr;

    /**
     * 业务id
     */
    private String businessKey;

    /**
     * 申请类型
     */
    private String applyType;

    /**
     * 合同签署类型-运行类-非运营类
     */
    private String contractSignType;

    /**
     * 审批结果-同意-驳回-终止流程
     */
    private String result;
    /**
     * 节点备注-审批意见
     */
    private String comment;

}
