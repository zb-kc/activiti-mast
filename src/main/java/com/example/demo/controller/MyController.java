package com.example.demo.controller;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.JsonUtil;
import com.example.demo.entity.KeyValue;
import com.example.demo.entity.MyForm;
import com.example.demo.entity.YwProcessInfoPO;
import com.example.demo.service.IActivitiService;
import com.example.demo.util.ActivitiImgUtils;
import com.example.demo.util.ActivitiUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.editor.constants.ModelDataJsonConstants;
import org.activiti.engine.*;
import org.activiti.engine.form.FormProperty;
import org.activiti.engine.form.StartFormData;
import org.activiti.engine.form.TaskFormData;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricDetail;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.impl.AbstractQuery;
import org.activiti.engine.impl.GroupQueryProperty;
import org.activiti.engine.impl.persistence.entity.HistoricDetailVariableInstanceUpdateEntity;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.Model;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.image.ProcessDiagramGenerator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sun.misc.BASE64Encoder;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Copyright (C), 2015-2019
 * FileName: MyController
 * Author:   MRC
 * Date:     2019/8/3 11:02
 * Description: 前端控制器
 * History:
 */

@Controller
@RequestMapping
public class MyController {

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private FormService formService;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private ProcessEngineConfiguration processEngineConfiguration;

    @Autowired
    private IActivitiService activitiService;


    @RequestMapping("index")
    public String toIndex(org.springframework.ui.Model model) {

        //根据id排序 字符串排序添加参数 "RES.ID_ +0"
        AbstractQuery aq = (AbstractQuery)repositoryService.createModelQuery();
        List list = aq.orderBy(new GroupQueryProperty("RES.ID_ +0")).asc().list();

        model.addAttribute("list", list);
        return "index";
    }


    /**
     * @return java.lang.String
     * @Author MRC
     * @Description  创建模型
     * @Date 11:10 2019/8/3
     * @Param []
     **/
    @RequestMapping("createModel")
    public String createModel(HttpServletRequest request, HttpServletResponse response) {

        String name = "请假流程";
        String description = "这是一个请假流程";

        String id = null;
        try {
            Model model = repositoryService.newModel();
            String key = name;
            //版本号
            String revision = "1";
            ObjectNode modelNode = objectMapper.createObjectNode();
            modelNode.put(ModelDataJsonConstants.MODEL_NAME, name);
            modelNode.put(ModelDataJsonConstants.MODEL_DESCRIPTION, description);
            modelNode.put(ModelDataJsonConstants.MODEL_REVISION, revision);

            model.setName(name);
            model.setKey(key);
            //模型分类 结合自己的业务逻辑
            //model.setCategory(category);

            model.setMetaInfo(modelNode.toString());

            repositoryService.saveModel(model);
            id = model.getId();

            //完善ModelEditorSource
            ObjectNode editorNode = objectMapper.createObjectNode();
            editorNode.put("id", "canvas");
            editorNode.put("resourceId", "canvas");
            ObjectNode stencilSetNode = objectMapper.createObjectNode();
            stencilSetNode.put("namespace",
                    "http://b3mn.org/stencilset/bpmn2.0#");
            editorNode.put("stencilset", stencilSetNode);
            repositoryService.addModelEditorSource(id, editorNode.toString().getBytes("utf-8"));

            response.sendRedirect(request.getContextPath() + "/static/modeler.html?modelId=" + id);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "index";
    }


    /**
     * @return java.lang.String
     * @Author MRC
     * @Description  部署一个模型
     * @Date 15:32 2019/8/3
     * @Param [id]
     **/
    @RequestMapping("deploymentModel")
    @ResponseBody
    public JSONObject deploymentModel(String id) throws Exception {

        String info = this.activitiService.deploymentProcess(id);

        return JsonUtil.getSuccessJson(info);
    }

    /**
     * @return java.lang.String
     * @Author MRC
     * @Description  开启流程页面
     * @Date 15:45 2019/8/3
     * @Param []
     **/
    @RequestMapping("startPage")
    public String startPage(org.springframework.ui.Model model) {
        //加载流程定义
        List<ProcessDefinition> list = repositoryService.createProcessDefinitionQuery().list();
        model.addAttribute("list", list);
        return "startPage";
    }


    /**
     * @return java.lang.String
     * @Author MRC
     * @Description  开启流程填写表单页面
     * @Date 16:47 2019/8/3
     * @Param []
     **/
    @RequestMapping("startProcess/{id}")
    public String startProcess(@PathVariable("id") String id, org.springframework.ui.Model model) {

        //按照流程定义ID加载流程开启时候需要的表单信息
        StartFormData startFormData = formService.getStartFormData(id);
        List<FormProperty> formProperties = startFormData.getFormProperties();

        //流程定义ID
        model.addAttribute("processesId", id);
        model.addAttribute("form", formProperties);

        return "startProcess";
    }

    @ResponseBody
    @RequestMapping("startProcess")
    public String startProcess(@RequestParam("businessKey") String businessKey ,@RequestParam("applyType") String applyType){

//        this.activitiService.startProcessInstance(businessKey , applyType , "");

        return "success";
    }

    @RequestMapping("/deleteTask/{id}")
    @ResponseBody
    public JSONObject deleteTask(@PathVariable("id") String deploymentId) {

        if (StringUtils.isEmpty(deploymentId)) {
            return JsonUtil.getFailJson("参数错误");
        }

        if("all".equals(deploymentId)){
            List<Deployment> list = repositoryService.createDeploymentQuery().list();
            for (Deployment deployment : list) {
                this.activitiService.deleteTaskProcessInstance(deployment.getId() , "测试删除");
            }
        }else{
            this.activitiService.deleteTaskProcessInstance(deploymentId , "测试删除");
        }

        return JsonUtil.getSuccessJson("流程已删除");

    }


    /**
     * @Description: 启动一个新的流程
     * @Param: [id]
     * @return: com.alibaba.fastjson.JSONObject
     * @Author: Mr.MRC
     * @Date: 2019/7/25  11:26
     */
    @RequestMapping("startProcesses")
    @ResponseBody
    public JSONObject startProcesses(@RequestParam Map<String, Object> param) {

        String processesId = (String) param.get("processesId");
        //流程提交人 这里模拟
        String businessKey = (String) param.get("userId");

        if (StringUtils.isEmpty(processesId)) {
            return JsonUtil.getFailJson("参数错误");
        }
        param.remove("processesId");

//        Execution last = runtimeService.createExecutionQuery().processInstanceBusinessKey(userId).processDefinitionId(processesId).singleResult();
//        if (null != last) {
//            return JsonUtil.getFailJson("请勿重复提交！");
//        }

        ProcessInstance pi = this.activitiService.startProcessInstance(processesId, businessKey, param);

        if (null == pi) {
            return JsonUtil.getFailJson("流程启动失败！");
        }
        return JsonUtil.getSuccessJson("启动流程成功！");

    }


    /**
     * @return java.lang.String
     * @Author MRC
     * @Description  跳转至审批页面
     * @Date 14:13 2019/9/7
     * @Param [id]
     **/
    @RequestMapping("taskApproval/{id}")
    public String toTaskList(@PathVariable("id") String id, org.springframework.ui.Model model) {

        if (StringUtils.isNotEmpty(id)) {

            List<Task> list = taskService.createTaskQuery().taskAssignee(id).list();
            model.addAttribute("list", list);
        }

        return "taskApproval";
    }

    /**
     * @return java.lang.String
     * @Author MRC
     * @Description  任务详情
     * @Date 14:45 2019/9/7
     * @Param [id, model]
     **/
    @RequestMapping("taskDetails/{taskId}")
    public String toTaskDetails(@PathVariable("taskId") String id, org.springframework.ui.Model model) {


        Map<String, Object> map = new HashMap<>(3);
        //当前任务
        Task task = this.taskService.createTaskQuery().taskId(id).singleResult();
        String processInstanceId = task.getProcessInstanceId();

        TaskFormData taskFormData = this.formService.getTaskFormData(id);
        List<FormProperty> list = taskFormData.getFormProperties();
        map.put("task", task);
        map.put("list", list);
        map.put("history", assembleProcessForm(processInstanceId));

        model.addAllAttributes(map);

        return "taskDetails";
    }


    /**
     * @return com.alibaba.fastjson.JSONObject
     * @Author MRC
     * @Description 完成任务
     * @Date 15:50 2019/9/7
     * @Param []
     **/
    @RequestMapping("completeTasks")
    @ResponseBody
    public JSONObject completeTasks(@RequestParam Map<String, Object> param) {


        String taskId = (String) param.get("taskId");

        if (StringUtils.isEmpty(taskId)) {
            return JsonUtil.getFailJson("参数错误");
        }
        param.remove("taskId");

        this.activitiService.completeTask(taskId , param);

        return JsonUtil.getSuccessJson("流程已确认");

    }

    @RequestMapping("completeTasks2")
    @ResponseBody
    public JSONObject completeTasks2(@RequestBody YwProcessInfoPO ywProcessInfoPO) {

        this.activitiService.completeTask(ywProcessInfoPO);

        return JsonUtil.getSuccessJson("流程已确认");

    }

    /**
     * 驳回任务
     * @param param taskId
     * @return JSONObject
     */
    @RequestMapping("/turndownTasks")
    @ResponseBody
    public JSONObject turndownTasks(@RequestParam Map<String , Object> param){

        String taskId = (String) param.get("taskId");

        if (StringUtils.isEmpty(taskId)) {
            return JsonUtil.getFailJson("参数错误");
        }
//        param.remove("taskId");
//
//        param.put("result" , "false");
//
//        activitiService.completeTask(taskId , param);

        activitiService.turndownTaskInstance(taskId , "材料缺失");

        return JsonUtil.getSuccessJson("测试驳回");
    }

    /**
     * 终止任务
     * @param param taskId
     * @return json
     */
    @RequestMapping("/endTask")
    @ResponseBody
    public JSONObject endTask(@RequestParam Map<String , Object> param){

        String taskId = (String) param.get("taskId");

        if (StringUtils.isEmpty(taskId)) {
            return JsonUtil.getFailJson("参数错误");
        }
        param.remove("taskId");

        activitiService.endTaskInstance(taskId , "测试终止任务");

        return JsonUtil.getSuccessJson("测试终止任务");
    }

    @RequestMapping("endTask2")
    @ResponseBody
    public JSONObject endTask(@RequestBody YwProcessInfoPO ywProcessInfoPO) {

        this.activitiService.terminateProcessInstance(ywProcessInfoPO);

        return JsonUtil.getSuccessJson("流程已终止");

    }

    /**
     * 获取bpmn文件
     * @return json
     * @throws IOException 文件下载异常
     */
    @RequestMapping("getPng/{name}")
    @ResponseBody
    public JSONObject getPng(@PathVariable("name") String name) throws IOException {
        ActivitiUtils.getDeployment(name);

        return JsonUtil.getSuccessJson("下载BPMN文件完毕");
    }


    /**
     * @return java.util.List<com.yckj.entity.MyForm>
     * @Author MRC
     * @Description 组装表单过程的表单信息
     * @Date 10:59 2019/8/5
     * @Param [processInstanceId]
     **/
    public List<MyForm> assembleProcessForm(String processInstanceId) {

        List<HistoricActivityInstance> historys = historyService.createHistoricActivityInstanceQuery().processInstanceId(processInstanceId).list();

        List<MyForm> myform = new ArrayList<>();

        for (HistoricActivityInstance activity : historys) {

            String actInstanceId = activity.getId();
            MyForm form = new MyForm();
            form.setActName(activity.getActivityName());
            form.setAssignee(activity.getAssignee());
            form.setProcInstId(activity.getProcessInstanceId());
            form.setTaskId(activity.getTaskId());
            //查询表单信息

            List<KeyValue> maps = new LinkedList<>();

            List<HistoricDetail> processes = historyService.createHistoricDetailQuery().activityInstanceId(actInstanceId).list();
            for (HistoricDetail process : processes) {
                HistoricDetailVariableInstanceUpdateEntity pro = (HistoricDetailVariableInstanceUpdateEntity) process;

                KeyValue keyValue = new KeyValue();

                keyValue.setKey(pro.getName());
                keyValue.setValue(pro.getTextValue());

                maps.add(keyValue);
            }
            form.setProcess(maps);

            myform.add(form);
        }

        return myform;
    }

    /**
     * @return com.alibaba.fastjson.JSONObject
     * @Author MRC
     * @Description  生成流程图
     * @Date 15:39 2019/9/7
     * @Param [processInstanceId]
     **/
    @RequestMapping("generateProcessImg")
    @ResponseBody
    public JSONObject generateProcessImg(String processInstanceId) throws IOException {

        //获取历史流程实例
        HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();
        //获取流程图
        BpmnModel bpmnModel = repositoryService.getBpmnModel(processInstance.getProcessDefinitionId());

        ProcessDiagramGenerator diagramGenerator = processEngineConfiguration.getProcessDiagramGenerator();
        ProcessDefinitionEntity definitionEntity = (ProcessDefinitionEntity) repositoryService.getProcessDefinition(processInstance.getProcessDefinitionId());

        List<HistoricActivityInstance> highLightedActivitList = historyService.createHistoricActivityInstanceQuery().processInstanceId(processInstanceId).list();

        ProcessInstance processInstanceRuning = runtimeService.createProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();
        //高亮环节id集合
        List<String> highLightedActivitis = new ArrayList<>();

        if(null != processInstanceRuning){
            //剔除驳回的节点 当前节点不高亮显示
            List<HistoricTaskInstance> list = historyService.createHistoricTaskInstanceQuery().processInstanceId(processInstanceId).orderByTaskId().asc().list();
            List<String> deleteTaskInstance = new ArrayList<>();

            Map<String , String > map = new HashMap<>();
            for (HistoricTaskInstance historicTaskInstance : list) {
                map.put(historicTaskInstance.getTaskDefinitionKey() , historicTaskInstance.getDeleteReason());
            }

            for(Map.Entry<String , String> m : map.entrySet()){
                String deleteReason = m.getValue();
                if (!"completed".equals(deleteReason)){
                    deleteTaskInstance.add(m.getKey());
                }
            }

            highLightedActivitList.removeIf(next -> deleteTaskInstance.contains(next.getActivityId()));
        }

        //高亮线路id集合
        List<String> highLightedFlows = ActivitiImgUtils.getHighLightedFlows(definitionEntity, highLightedActivitList);

        for (HistoricActivityInstance tempActivity : highLightedActivitList) {
            String activityId = tempActivity.getActivityId();
            highLightedActivitis.add(activityId);
        }

        //配置字体
        InputStream imageStream = diagramGenerator.generateDiagram(bpmnModel, "png", highLightedActivitis, highLightedFlows, "宋体", "微软雅黑", "黑体", null, 2.0);

        BufferedImage bi = ImageIO.read(imageStream);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(bi, "png", bos);
        //转换成字节
        byte[] bytes = bos.toByteArray();
        BASE64Encoder encoder = new BASE64Encoder();
        //转换成base64串
        String pngBase64 = encoder.encodeBuffer(bytes);
        //删除 \r\n
        pngBase64 = pngBase64.replaceAll("\n", "").replaceAll("\r", "");

        bos.close();
        imageStream.close();
        return JsonUtil.getSuccessJson("success", pngBase64);
    }

    /**
     * 上传工作流zip压缩包 部署流程
     * @param file2 zip压缩包
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/updateActivitiZip", method = RequestMethod.POST)
    public String updateFile(@RequestParam("file")  MultipartFile file2){

        return this.activitiService.updateFile(file2);
    }


}