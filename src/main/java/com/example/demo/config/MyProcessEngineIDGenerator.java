package com.example.demo.config;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import org.activiti.engine.impl.cfg.IdGenerator;
import org.springframework.stereotype.Component;

/**
 * 工作流id生成器
 * @author ZB3436.xiongshibao
 * @date 2021/11/23
 */
@Component
public class MyProcessEngineIDGenerator implements IdGenerator {
    @Override
    public String getNextId() {
        return String.valueOf(IdWorker.getId());
    }
}
