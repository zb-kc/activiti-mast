package com.example.demo.config;

import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 重写工作流自动部署
 */
@Component
@Slf4j
public class MyActivitiAutoDeployment implements ApplicationRunner {

    @Autowired
    private RepositoryService repositoryService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("====activiti自动部署校验，首次部署以及部署修改过的文件，开始====");

        Map<String , InputStream> deploymentMap = new HashMap<>();
        Map<String , Boolean> fileNameStatusMap = new HashMap<>();

        //获取数据库已部署流程
        List<ProcessDefinition> list = repositoryService.createProcessDefinitionQuery().list();
        if(!list.isEmpty()){
            Map<String , ProcessDefinition> processDefinitionMap = new HashMap<>();
            for (ProcessDefinition processDefinition : list) {
                String key = processDefinition.getKey();
                if (processDefinitionMap.containsKey(key)){
                    int version = processDefinition.getVersion();

                    ProcessDefinition processDefinitionTemp = processDefinitionMap.get(key);
                    int versionTemp = processDefinitionTemp.getVersion();

                    if(versionTemp > version){
                        processDefinitionMap.put(key , processDefinitionTemp);
                        continue;
                    }
                }

                processDefinitionMap.put(key , processDefinition);
            }


            for (ProcessDefinition processDefinition : processDefinitionMap.values()) {
                String deploymentId = processDefinition.getDeploymentId();
                String bpmnName = processDefinition.getResourceName();

                String keyName = replaceFileName(bpmnName);

                InputStream bpmnInputStream = repositoryService.getResourceAsStream(deploymentId, bpmnName);
                deploymentMap.put(keyName, bpmnInputStream);
            }
        }


        //获取项目中的流程定义文件
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        try {
            Resource[] oldResources = resolver.getResources("classpath*:/processes/**.bpmn");
            for (Resource oldResource : oldResources) {
                if(oldResource.isFile()){

                    String filename = oldResource.getFilename();
                    String keyName = replaceFileName(filename);

                    InputStream mapInputStream = deploymentMap.get(keyName);
                    if(null == mapInputStream){
                        fileNameStatusMap.put(oldResource.getFilename() , false);
                        continue;
                    }
                    String oldResourceFileMd5 = DigestUtils.md5DigestAsHex(mapInputStream);
                    String newResourceFileMd5 = DigestUtils.md5DigestAsHex(oldResource.getInputStream());

                    fileNameStatusMap.put(oldResource.getFilename() , oldResourceFileMd5.equals(newResourceFileMd5));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(!fileNameStatusMap.isEmpty()){
            for(Map.Entry<String , Boolean> temp : fileNameStatusMap.entrySet()){
                if(!temp.getValue()){
                    String key = temp.getKey();
                    Deployment deploy = repositoryService.createDeployment()
                            .name(key)
                            .addClasspathResource("processes/" + key)
                            .deploy();

                    log.info(key + "流程重新部署id:" + deploy.getId());
                }
            }
        }

        log.info("====activiti自动部署校验，首次部署以及部署修改过的文件，结束====");
    }

    private String replaceFileName(String filename) {
        if(filename.isEmpty()){
            return filename;
        }

        String keyName;
        if (filename.contains("/")) {
            String[] split = filename.split("/");
            keyName = split[split.length - 1];
        } else {
            String[] newFileName = filename.split("\\\\");
            keyName = newFileName[newFileName.length - 1];
        }
        return keyName;
    }
}
