package com.example.demo;

import com.example.demo.entity.YwProcessInfoPO;
import org.thymeleaf.util.ListUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Test {

    public static void main(String[] args) {
         Map<String , String> keyMap;
        {
            keyMap = new HashMap<>();
            keyMap.put("businessType" , "业务类型");
            keyMap.put("agencyType" , "申请类型");
            keyMap.put("id" , "序号");
            keyMap.put("proName" , "项目名称");
            keyMap.put("acceptCode" , "受理号");
            keyMap.put("stageName" , "阶段名称");
            keyMap.put("applicantUnit" , "申请单位");
            keyMap.put("process" , "流程进度");
            keyMap.put("applicationTime" , "申请时间");
            keyMap.put("contactName" , "联系人");
            keyMap.put("contactPhone" , "联系电话");
            keyMap.put("type" , "联系电话");
        }

            for(Map.Entry<String , String> map :keyMap.entrySet()){
                System.out.println(map.getKey() +"=" +map.getValue());
            }


    }

    public static List<YwProcessInfoPO> test1() {
        List<YwProcessInfoPO> queryList = new ArrayList<>();

        YwProcessInfoPO ywProcessInfoPO1 = new YwProcessInfoPO();
        YwProcessInfoPO ywProcessInfoPO2 = new YwProcessInfoPO();
        YwProcessInfoPO ywProcessInfoPO3 = new YwProcessInfoPO();
        YwProcessInfoPO ywProcessInfoPO4 = new YwProcessInfoPO();
        YwProcessInfoPO ywProcessInfoPO5 = new YwProcessInfoPO();
        YwProcessInfoPO ywProcessInfoPO6 = new YwProcessInfoPO();
        YwProcessInfoPO ywProcessInfoPO7 = new YwProcessInfoPO();
        YwProcessInfoPO ywProcessInfoPO8 = new YwProcessInfoPO();

        ywProcessInfoPO1.setActName("一级审核");
        ywProcessInfoPO2.setActName("申请");
        ywProcessInfoPO3.setActName("一级审核");
        ywProcessInfoPO4.setActName("二级审核");
        ywProcessInfoPO5.setActName("申请");
        ywProcessInfoPO6.setActName("一级审核");
        ywProcessInfoPO7.setActName("二级审核");
        ywProcessInfoPO8.setActName("三级审核");

        ywProcessInfoPO1.setStartTimeStr("2021-09-17 11:25:57.975");
        ywProcessInfoPO2.setStartTimeStr("2021-09-17 11:26:07.207");
        ywProcessInfoPO3.setStartTimeStr("2021-09-17 11:27:09.881");
        ywProcessInfoPO4.setStartTimeStr("2021-09-17 11:27:18.910");
        ywProcessInfoPO5.setStartTimeStr("2021-09-17 11:27:31.280");
        ywProcessInfoPO6.setStartTimeStr("2021-09-17 11:29:23.141");
        ywProcessInfoPO7.setStartTimeStr("2021-09-17 11:29:32.290");
        ywProcessInfoPO8.setStartTimeStr("2021-09-17 11:29:38.944");

        queryList.add(ywProcessInfoPO1);
        queryList.add(ywProcessInfoPO2);
        queryList.add(ywProcessInfoPO3);
        queryList.add(ywProcessInfoPO4);
        queryList.add(ywProcessInfoPO5);
        queryList.add(ywProcessInfoPO6);
        queryList.add(ywProcessInfoPO7);
        queryList.add(ywProcessInfoPO8);

        queryList.add(ywProcessInfoPO1);

        if (ListUtils.isEmpty(queryList)) {
            return new ArrayList<>();
        }

        Map<String, YwProcessInfoPO> queryMapTemp = new HashMap<>();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            //对list去重筛选 选出START_TIME最大的同名节点
            for (YwProcessInfoPO ywProcessInfoPO : queryList) {
                String actName = ywProcessInfoPO.getActName();
                String startTimeStr = ywProcessInfoPO.getStartTimeStr();

                if (queryMapTemp.containsKey(actName)) {
                    YwProcessInfoPO ywProcessInfoPOTemp = queryMapTemp.get(actName);
                    String startTimeStrTemp = ywProcessInfoPOTemp.getStartTimeStr();

                    long startTime = sdf.parse(startTimeStr).getTime();
                    long startTimeTemp = sdf.parse(startTimeStrTemp).getTime();
                    if (startTime > startTimeTemp) {
                        queryMapTemp.put(actName, ywProcessInfoPO);
                    }
                } else {
                    queryMapTemp.put(actName, ywProcessInfoPO);
                }
            }

            return new ArrayList<>(queryMapTemp.values());

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return new ArrayList<>();
    }

}
