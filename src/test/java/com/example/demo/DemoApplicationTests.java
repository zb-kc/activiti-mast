package com.example.demo;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.ProcessDefinition;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

	@Test
	public void contextLoads() {
	}

	/**
	 * 下载 资源文件
	 * 方案1： 使用Activiti提供的api，来下载资源文件,保存到文件目录
	 * 方案2： 自己写代码从数据库中下载文件，使用jdbc对blob 类型，clob类型数据读取出来，保存到文件目录
	 * 解决Io操作：commons-io.jar
	 * 这里我们使用方案1，Activiti提供的api：RespositoryService
	 */
	@Test
	public void getDeployment() throws IOException {
//         1、得到引擎
		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        2、获取api，RepositoryService
		RepositoryService repositoryService = processEngine.getRepositoryService();
//        3、获取查询对象 ProcessDefinitionQuery查询流程定义信息
		ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
				.processDefinitionKey("会签流程测试")
				.singleResult();
//        4、通过流程定义信息，获取部署ID
		String deploymentId = processDefinition.getDeploymentId();
//        5、通过RepositoryService ，传递部署id参数，读取资源信息（png 和 bpmn）
//          5.1、获取png图片的流
//        从流程定义表中，获取png图片的目录和名字
		String pngName = processDefinition.getDiagramResourceName();
//        通过 部署id和 文件名字来获取图片的资源
		InputStream pngInput = repositoryService.getResourceAsStream(deploymentId, pngName);
//          5.2、获取bpmn的流
		String bpmnName = processDefinition.getResourceName();
		InputStream bpmnInput = repositoryService.getResourceAsStream(deploymentId, bpmnName);
//        6、构造OutputStream流
		File pngFile = new File("e:/evectionflow02.png");
		File bpmnFile = new File("e:/evectionflow02.bpmn");
		FileOutputStream pngOutStream = new FileOutputStream(pngFile);
		FileOutputStream bpmnOutStream = new FileOutputStream(bpmnFile);
//        7、输入流，输出流的转换
		IOUtils.copy(pngInput,pngOutStream);
		IOUtils.copy(bpmnInput,bpmnOutStream);
//        8、关闭流
		pngOutStream.close();
		bpmnOutStream.close();
		pngInput.close();
		bpmnInput.close();
	}

}
